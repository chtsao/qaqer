+++
title = '學霸其實不是學霸'
date = 2023-11-24T08:41:38+08:00
draft = false
tags = ["學霸"]
math = true

+++

#### 前奏

* 你在聽誰談/為什麼值得談
* 一個**定義**:   三個**觀點**：一個**結語** 

### 定義：學霸 $\stackrel{{def}}{=}$ 在某領域/技能 (學) 制霸的人; 達人, 專家

### 觀點

1. 學霸是一現在式，進行式，甚至是未來式；而不是完成式，過去式。他有可能是一時的網紅，但更重要的刻劃是他是一位持續的 KOL. 例：愛因斯坦。
2. 學霸 $\neq$ 天才；天才 $\neq$ 高IQ. 學霸多半是學來的，學霸通常不是天才，反過來也是如此。過份強調天才，對學習者的成長或社會的健全都不太正向。例：丘成桐 on 天才
3. 學霸是(單一領域)專家 $\neq$ 全能； 協槓有可能，但是不必然；及便有也不見得非常多項。但反過來說，學霸通常對很多領域都很有興趣。這對他們持續的創造力與學習有很重要的影響。例：博士不博、達文西。

### 結語 

"學者"起始設定。申請東華的門檻條件. 兩好(好奇、好學) 三Why + 持續。相關主題：學習

### 延伸閱讀

1. [Michael Balter (2012): Why Einstein Was a Genius. News @ Science](https://www.science.org/content/article/why-einstein-was-genius)
2. [丘成桐: 我學數學40年，還沒有看過天才](http://www.tta.tp.edu.tw/1_news/detail.asp?titleid=1020)
3. [專訪數學家丘成桐@澎湃 (2016)](https://www.thepaper.cn/newsDetail_forward_1418181)
4. [What Made Leonardo da Vinci a Genius?@National.Geographic. Simon Worrall (2017)](https://www.nationalgeographic.com/history/article/leonardo-da-vinci-genius-walter-isaacson)
5. [Animal Schoold by Reavis, G. H.](https://ia802602.us.archive.org/20/items/TheAnimalSchoolIllustrate-English-GeorgeReavis/TheAnimalSchool_georgeReavis_illustrated.pdf)
